#pragma once

#include <map>
#include <string>

#include <SDL\SDL.h>

class ResourcesManager
{
	std::map<std::string, SDL_Surface*> surfaces;
	std::map<std::string, SDL_Texture*> textures;

	std::map<std::string, std::string> resourcesPath;

	SDL_Renderer *renderer;

public:
	ResourcesManager();
	~ResourcesManager();

	void initialize(SDL_Renderer *_renderer);

	bool loadBMP(std::string name, std::string const& path, bool loadSurface = false);
	bool loadBMP(std::string name, std::string const& pathName, std::string const& fileName, bool loadSurface = false);
	void addResourcesPath(std::string name, std::string path);

	SDL_Texture *getTexture(std::string const& name);
	SDL_Surface *getSurface(std::string const& name);
};

