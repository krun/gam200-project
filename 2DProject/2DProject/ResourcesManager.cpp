#include "ResourcesManager.hh"


ResourcesManager::ResourcesManager()
{
}

void ResourcesManager::initialize(SDL_Renderer *_renderer)
{
	renderer = _renderer;
}

ResourcesManager::~ResourcesManager()
{
	for each (auto var in surfaces)
		SDL_FreeSurface(var.second);
	for each (auto var in textures)
		SDL_DestroyTexture(var.second);
}


bool ResourcesManager::loadBMP(std::string name, std::string const& path, bool loadSurface)
{
	SDL_Surface *surf = SDL_LoadBMP(path.c_str());

	if (surf == nullptr)
		return false;

	SDL_Texture *tex = SDL_CreateTextureFromSurface(renderer, surf);

	if (tex == nullptr)
		return false;
	textures[name] = tex;
	if (loadSurface)
		surfaces[name] = surf;
	else
		SDL_FreeSurface(surf);
	return true;
}

bool ResourcesManager::loadBMP(std::string name, std::string const& pathName, std::string const& fileName, bool loadSurface)
{
	return loadBMP(name, this->resourcesPath[name] + '\\' + fileName, loadSurface);
}

void ResourcesManager::addResourcesPath(std::string name, std::string path)
{
	this->resourcesPath[name] = path;
}

SDL_Texture *ResourcesManager::getTexture(std::string const& name)
{
	return textures[name];
}

SDL_Surface *ResourcesManager::getSurface(std::string const& name)
{
	return surfaces[name];
}
