#pragma once

#include <stack>

#include <SDL\SDL.h>

#include "Vector2.hpp"

class Input
{
	SDL_Event e;
	std::stack<SDL_Keycode> keys;


public:
	Input();
	~Input();
	void initialize();

	void update();

	bool exit;
	int mouseButton;
	Vector2<int> mousePosition;
	SDL_Keycode getLastKey(bool removeIt = false);
};

