#pragma once

#include <SDL/SDL.h>

class Clock
{
	int globalTime;
	int deltaTime;

public:
	Clock();
	~Clock();
	void initialize();
	void update();
	int getDeltaTime();
	int getGlobalTime();
};

