#pragma once

#include <iostream>
#include <string>

#include <SDL\SDL.h>

#include "Clock.hh"
#include "Input.hh"
#include "ResourcesManager.hh"
#include "Scene.hh"


class GameEngine
{
public:
	GameEngine();
	~GameEngine();
	bool	initialize(std::string const& windowName, int x = 100, int y = 100, int w = 640, int h = 480, Uint32 SDLFlags = SDL_WINDOW_SHOWN);

	int		loop();

	std::string error;

private:

	Clock clock;
	Input input;
	ResourcesManager resources;

	bool update();
	void draw();

	SDL_Window *window;
	SDL_Renderer *renderer;
};

