#include "Clock.hh"


Clock::Clock()
{
}

void Clock::initialize()
{
	globalTime = SDL_GetTicks();
}

void Clock::update()
{
	deltaTime = globalTime - SDL_GetTicks();
	globalTime = SDL_GetTicks();
}

int Clock::getDeltaTime()
{
	return deltaTime;
}

int Clock::getGlobalTime()
{
	return globalTime;
}

Clock::~Clock()
{
}
