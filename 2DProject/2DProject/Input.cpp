#include "Input.hh"


Input::Input() : exit(false), mouseButton(0)
{
}

void Input::initialize()
{
}

Input::~Input()
{
}

void Input::update()
{
	while (SDL_PollEvent(&e))
	{
		if (e.type == SDL_QUIT)
			exit = true;
		else if (e.type == SDL_KEYDOWN)
			keys.push(e.key.keysym.sym);
		else if (e.type == SDL_MOUSEBUTTONDOWN)
			mouseButton = e.button.button;
		else if (e.type == SDL_MOUSEBUTTONUP)
			mouseButton = -1;
		SDL_GetMouseState(&mousePosition.x, &mousePosition.y);
	}
}

SDL_Keycode Input::getLastKey(bool removeIt)
{
	if (!keys.empty())
	{
		auto tmp = keys.top();

		if (removeIt)
			keys.pop();

		return tmp;
	}
	return -1;
}
