#include "GameEngine.hh"

GameEngine::GameEngine()
{
}

bool	GameEngine::initialize(std::string const& windowName, int x, int y, int w, int h, Uint32 SDLFlags)
{
	if (SDL_Init(SDL_INIT_VIDEO) == -1)
	{
		error = "Couldn't initialize SDL : " + std::string(SDL_GetError());
		return false;
	}

	window = SDL_CreateWindow(windowName.c_str(), x, y, w, h, SDLFlags);

	if (window == nullptr)
	{
		error = "Fail to Create new Window : " + std::string(SDL_GetError());
		return false;
	}

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	if (renderer == nullptr)
	{
		error = "Fail to create renderer : " + std::string(SDL_GetError());
		return false;
	}

	input.initialize();
	clock.initialize();
	resources.initialize(renderer);

	return true;
}

int		GameEngine::loop()
{
	while (input.exit == false)
	{
		clock.update();
		input.update();
		update();
		draw();
	}
	return 0;
}

bool GameEngine::update()
{
	return true;
}


void GameEngine::draw()
{
}


GameEngine::~GameEngine()
{
	SDL_DestroyWindow(window);
	SDL_Quit();
}